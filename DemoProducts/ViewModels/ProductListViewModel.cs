﻿using DemoProducts.Core.Models;
using DemoProducts.Core.Services;
using DemoProducts.Services;
using DemoProducts.Views;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Uwp.UI.Animations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DemoProducts.ViewModels
{
    public class ProductListViewModel : ObservableObject
    {
        private ICommand _itemClickCommand;

        public ICommand ItemClickCommand => _itemClickCommand ?? (_itemClickCommand = new RelayCommand<Product>(OnItemClick));

        public ObservableCollection<Product> Source { get; } = new ObservableCollection<Product>();

        public ProductListViewModel()
        {
        }

        public async Task LoadDataAsync()
        {
            Source.Clear();


            var data = await ProductServices.GetContentProductListGrid();

            foreach (var item in data)
            {
                Source.Add(item);
            }
        }

        private void OnItemClick(Product clickedItem)
        {
            if (clickedItem != null)
            {
                NavigationService.Frame.SetListDataItemForNextConnectedAnimation(clickedItem);
                NavigationService.Navigate<ProductListItemPage>(clickedItem.Id);
            }
        }

    }
}
