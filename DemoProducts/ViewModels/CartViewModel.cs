﻿using DemoProducts.Core.Models;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;

namespace DemoProducts.ViewModels
{
    public class CartViewModel : ObservableObject
    {
        public ObservableCollection<Cart> Source { get; } = new ObservableCollection<Cart>();
        public ObservableCollection<Product> SourceItems { get; } = new ObservableCollection<Product>();

        public CartViewModel()
        {

        }

    }
}
