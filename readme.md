# Demo para una aplicaci�n Universal Windows Plataform (UWP)

## Descripci�n:

Desarrollar una aplicaci�n **UWP ( C# & XAML)** para una Tienda virtual, que incluya una pantalla con lista de productos donde solo puede elegir la cantidad de productos a comprar, proceder al pago, simular el cobro e imprimir un ticket de compra(simular en alguna pantalla el dise�o din�mico). 

**Nota: Enfoque en Data-Binding**

## Caracter�sticas de la aplicaci�n:

- 
  Simular el cobro.

- 
  Total a pagar.

- Pago.

- Cambio.

- Bot�n Pagar (Validando importes).

- 
  Ticket.- Pantalla Final.

- 
  Encabezado �Coppel� (centrado).

- Listado de art�culos (cantidad - art�culo - precio - importe).

- Total.

- Pie de ticket �Coppel mejora tu vida� (centrado).

- 
  Forzoso el utilizar un control de lista.


 **Consumir  API dummy :**

[API fake de platzi]( https://fakeapi.platzi.com/)

[Api Products Platzi](https://api.escuelajs.co/api/v1/products)

------

## Consideraciones a seguir

1. Aplicar **Patr�n de dise�o MVVM** en el proyecto.
2. Aplicar **Patr�n de dise�o MVC** en la soluci�n.
3. Consumo de **API Rest** con las *mejores pr�cticas* y utilizar headers.
4. Aplicar mejores pr�cticas de codificaci�n.