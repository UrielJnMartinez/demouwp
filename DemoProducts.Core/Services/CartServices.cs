﻿using DemoProducts.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DemoProducts.Core.Services
{
    public static class CartServices
    {
        private static Cart _cartContent;
        private static Cart GetCartContent()
        {
            return _cartContent;
        }

        //total products count
        private static int GetTptalProductCart()
        {
            return _cartContent.TotalProducts;
        }

        private static async Task<Cart> AddContentCart(Cart cart)
        {
            // add new product to cart
            _cartContent.TotalProducts += cart.TotalProducts;
            _cartContent.Total += cart.Total;


            await Task.CompletedTask;
            return _cartContent;

        }
    }
}
