﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoProducts.Core.Models;


namespace DemoProducts.Core.Services
{
    public static class ProductServices
    {
        private static IEnumerable<Product> _allOrders;

        private static async Task<IEnumerable<Product>> AllProducts()
        {
            HttpProductServices httpProductServices = new HttpProductServices("https://api.escuelajs.co/api/v1");
            var apiProducts = await httpProductServices.GetAsync<Product>("products?offset=0&limit=10");

            return apiProducts;
        }

        public static async Task<IEnumerable<Product>> GetContentProductListGrid()
        {
            if (_allOrders == null)
            {
                _allOrders = await AllProducts();
            }

            await Task.CompletedTask;
            return _allOrders;
        }
    }
}
