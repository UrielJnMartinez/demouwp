﻿using DemoProducts.Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DemoProducts.Core.Services
{
    public class HttpProductServices
    {
        private readonly Dictionary<string, object> responseCache;
        private HttpClient client;

        public HttpProductServices(string defaultBaseUrl = "")
        {
            client = new HttpClient();

            if (!string.IsNullOrEmpty(defaultBaseUrl))
            {
                client.BaseAddress = new Uri($"{defaultBaseUrl}/");
                var headers = client.DefaultRequestHeaders;
                string header = "ie";
                if (!headers.UserAgent.TryParseAdd(header))
                {
                    throw new Exception("Invalid header value: " + header);
                }

                header = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)";
                if (!headers.UserAgent.TryParseAdd(header))
                {
                    throw new Exception("Invalid header value: " + header);
                }
            }

            responseCache = new Dictionary<string, object>();
        }

        public async Task<List<T>> GetAsync<T>(string uri, string accessToken = null, bool forceRefresh = false)
        {
           List<T> result = default;

            if (forceRefresh || !responseCache.ContainsKey(uri))
            {
                // AddAuthorizationHeader(accessToken);
                var json = await client.GetStringAsync(uri);
                result = await Task.Run(() => JsonConvert.DeserializeObject<List<T>>(json.ToString()));

                if (responseCache.ContainsKey(uri))
                {
                    responseCache[uri] = result;
                }
                else
                {
                    responseCache.Add(uri, result);
                }
            }
            else
            {
                result = (List<T>)responseCache[uri];
            }

            return result;
        }

    }
}
