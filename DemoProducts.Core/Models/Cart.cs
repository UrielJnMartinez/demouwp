﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoProducts.Core.Models
{
    public class Cart
    {
        public int TotalProducts { get; set; }

        public double Subtotal { get; set; }

        public double Total { get; set; }

        public double PaymentReceived { get; set; }

        public double Exchange { get; set; }

        public List<Product> ProductsItem{ get; set;}

    }
}
