﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoProducts.Core.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }

        public List<String> images { get; set; }

        public string CreationAt { get; set; }

        public string UpdatedAt { get; set; }

        public ProductCategory Category { get; set; }
    }

    public class ProductCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public string CreationAt { get; set; }

        public string UpdatedAt { get; set; }
    }
}
